

# 对于12月13日参加的第三届许昌市青少年机器人竞赛使用到的模块详解及赛后感想

___________________________________________________________________________________________________________________________________

## 此次参加的赛类为机器人创意类A类赛事，此次赛事作品项目名称为“喵喵解压器”

<img src="image/喵喵解压器全身图.jpg" alt="喵喵解压器全身图" style="zoom: 33%;" />

----

## 次使用主控板为Arduino UNO及拓展版

### 主板

<img src="image/Arduino UNO主控板.jpg" style="zoom: 33%;" />

### 扩展版

<img src="image/Arduino UNO拓展板.jpg" style="zoom: 33%;" />

---

## 眼部使用超声波传感器

<img src="image/超声波传感器.jpg" style="zoom: 25%;" />

### 超声波传感器接线

> ​	   VCC	:		V（电源+极）
> ​		 Trig	:		数字接口（输出信号）
> ​	   Echo	:		数字接口(输入信号)
> ​	   GND	:		GND(接地-极)

![超声波传感器接线](image/超声波传感器接线.jpg)

### 超声波传感器运行代码：

```c
const int TrigPin = 4; //定义输出引脚为D4
const int EchoPin = 5; //定义输入引脚为D5

int LedPin = 12;//定义LED灯为D12引脚
float cm; 

void setup() 
{ 
    Serial.begin(9600); //波特率为9600
    pinMode(TrigPin, OUTPUT); //初始化TrigPin为输出针脚
    pinMode(EchoPin, INPUT); //初始化EchoPin为输入针脚
    pinMode(8, OUTPUT);//初始化D8号针脚
    pinMode(LedPin, OUTPUT);//初始化LED灯引脚为输出引脚
} 

void loop() 
{ 
    digitalWrite(8, LOW);//给8号针脚一个低电平

    digitalWrite(TrigPin, LOW); //以低电平发一个短时间脉冲去TrigPin 
    delayMicroseconds(2); //延时2微秒
    digitalWrite(TrigPin, HIGH); //以高电平发出一个短时间脉冲去TrigPin
    delayMicroseconds(10); //延时10微秒
    digitalWrite(TrigPin, LOW); //以低电平发出一个脉冲去TrigPin
    cm = pulseIn(EchoPin, HIGH) / 58.0; //将回波时间换算成cm 
    cm = (int(cm * 100.0)) / 100.0; //保留两位小数 
    if (cm >= 2 && cm <= 10)//如果距离>=2cm与<=10cm
    {
        digitalWrite(8, HIGH);//8D发出一个高电平
        digitalWrite(LedPin, HIGH);}//在距离范围内亮灯
    else{//否则
        digitalWrite(LedPin, LOW);//发出低电平LED灯灭
    }
}
```

--------

## 头部使用9G-180°舵机控制摇头

<img src="image/头部舵机.jpg" style="zoom: 25%;" />

### 舵机接线：

>   ​	  橙（或黄）： 数字信号接口
>  ​				   红  ： VCC（电源+极）
>  ​		           棕  ： GND（接地-极）

![](image/9g舵机接线.jpg)



### 舵机运行代码

```c
#include <Servo.h> 
 
Servo myservo;
                 
int pos = 0;    // 用变量储存每次变化的度数
 
void setup() 
{ 
  myservo.attach(9);  // 初始化9号引脚
} 
 
 
void loop() 
{ 
  for(pos = 0; pos < 180; pos += 1)  // 舵机从0度旋转至180，每次旋转1度
  {                                  
    myservo.write(pos);              //  让舵机转到变量中的位置
    delay(15);                       // 设置延时15微秒
  } 
  for(pos = 180; pos>=1; pos -= 1)     // 舵机从180度旋转至0度
  {                                
    myservo.write(pos);              // 让舵机转到变量中的位置 
    delay(15);                       // 设置延时15微秒 
  } 
} 
```

---

## 头部后方安装的mp3播放器

<img src="image/Mp3播放器-1608024276016.jpg" style="zoom:25%;" />

### mp3接线图

> ​       GND	：	GND
> ​		VCC	：	VCC

（Mp3物理按键控制播放所以其余接口不接）

### Mp3运行代码：（由于Mp3为独立模块所以本次项目未建立Mp3运行代码）

---

## 实时温度检测器（由于温度检测器为独立仪器所以木有接线图）

<img src="image/温度监控器.jpg" style="zoom:25%;" />

<img src="image/实时温度检测仪.jpg" style="zoom:25%;" />

---

## 腿部LED-WS2812-30彩色灯带

<img src="image/LEDWS2812-30彩色LED灯带-1608024761801.jpg" style="zoom:25%;" />

### LED-WS2812-30彩色灯带接线

>  ​		红	：	VCC（电源+极）
> ​		黑	：	GND(接地-极)
> ​		蓝	：	DO（数字信号）



![LEDWS2812-30彩色LED灯带接线](image/LEDWS2812-30彩色LED灯带接线.png)

### LEDWS2812-30彩色灯带代码

```c
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
	#include <avr/power.h>
#endif
#define PIN 7//定义7号针脚

Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_GRB + NEO_KHZ800); 

void setup()//初始化 
{
	#if defined (__AVR_ATtiny85__)
		if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
     #endif
     strip.begin();
     strip.show();
}
 
void loop() {
  int i = 0;//定义一个i变量记录每次LED灯珠的闪烁
  for (i = 0; i < 30; i++)//从0开始每次递增1
  {
     strip.setPixelColor(i,random(10 , 255),random(100 , 255), 0);//设定LED灯的颜色
     strip.show();
     delay(100);//延时微秒
  }
  for(i = 0; i < 30; i++)
  {
     strip.setPixelColor(i , 0 , 0 , 0);//LED灯的颜色变化
     strip.show();
  }
}
```

## 至此喵喵解压器所用元器件已介绍完毕

-----

## 参赛感受

s此次评委老师主要是围绕着“为什么要做这个项目”，“做这个项目都使用了什么”，“做这个项目所要达到的目的”来对此次项目做的介绍。还提问了“各个传感器用的是几伏电运行的”，“每种传感器都接的什么输入输出接口（如9g舵机需要连接数字接口）”，“猫咪身上的各种传感器的名称”。

经过这次比赛，我感觉我打字能力逐渐增强，已经快要进入盲打的地步了。

---

